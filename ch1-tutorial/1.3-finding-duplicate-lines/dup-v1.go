// dup-v1 prints the text of each line that appears more than once in the standard input, preceded by its count.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() { // input.Scan() reads the next line and removes the newline character at the end
		counts[input.Text()]++
	}
	// NOTE: Ignoring potential errors from input.Err()
	for key, value := range counts {
		if value > 1 {
			fmt.Printf("%d\t%s\n", value, key)
		}
	}
}
