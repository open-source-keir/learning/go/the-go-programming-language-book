// echo-v1 prints it's command-line arguments
package main

import (
	"fmt"
	"os"
)

func main() {
	// Declare several string type variables, initialises to it's 'zero value' (0 for numeric, "" for string)
	var s, sep string
	// for initialisation; condition; post {}
	for i := 1; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)
}
