package main

import (
	"fmt"
)

func main() {
	// Result of calling coinFlip() is compared to the value of each case.
	// Cases do not fall through from one to the next as in C-like languages (can use rarely uses fallthrough statement)
	switch sigNum(0) {
	case +1:
		fmt.Println("Heads")
	case -1:
		fmt.Println("Tails")
	default:
		fmt.Println("Landed on edge!")
	}
}

func sigNum(x int) int {
	// 'Tagless' switch statement - does not require an operand, it can just list the cases
	switch {
	case x > 0:
		return +1
	case x < 0:
		return -1
	default:
		return 0
	}
}
