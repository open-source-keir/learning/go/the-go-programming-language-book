package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var palette = []color.Color{color.White, color.Black}

const (
	whiteIndex = 0 // First colour in palette
	blackIndex = 1 // Next colour in palette
)

func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		cycles = 5  // number of complete x oscillator revolutions
		resolution = 0.0001  // angular resolution
		size = 100  // image canvas covers [-size..+size]
		nFrames = 64  // number of animation frames
		delay = 8  // delay between frames in 10ms units
	)

	frequency := rand.Float64() * 3.0 // relative frequency of y oscillator
	animation := gif.GIF{LoopCount: nFrames}
	phase := 0.0 // phase delta
	for i := 0; i < nFrames; i++ {
		rectangle := image.Rect(0,0,2*size+1, 2*size+1)
		image := image.NewPaletted(rectangle, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += resolution {
			x := math.Sin(t)
			y := math.Sin(t*frequency + phase)
			image.SetColorIndex(size+int(x*size+0.5), size+int(y*size + 0.5), blackIndex)
		}
		phase += 0.1
		animation.Delay = append(animation.Delay, delay)
		animation.Image = append(animation.Image, image)
	}
	gif.EncodeAll(out, &animation) // NOTE: IGNORING ENCODING ERRORS
}
